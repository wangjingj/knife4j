package com.github.xiaoymin.knife4j.spring.plugin;

import com.fasterxml.classmate.TypeResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.OperationModelsProviderPlugin;
import springfox.documentation.spi.service.contexts.RequestMappingContext;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE + 20)
public class OperationMockRulePlugin implements OperationModelsProviderPlugin {
    @Autowired
    private TypeResolver typeResolver;

    @Override
    public void apply(RequestMappingContext context) {
//        Class<?> clazz = null;
//        try {
//            clazz = Class.forName("com.jykj.demo.vo.Test");
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//        ResolvedType modelType = context.alternateFor(typeResolver.resolve(clazz));
////        context.operationModelsBuilder().addReturn(modelType);
//        context.operationModelsBuilder().addInputParam(modelType);
//        System.out.println(context.getModelMap());
        context.getModelMap().forEach((s, model) -> {
            model.getProperties().forEach((a, b) -> {
                System.out.println(b.getName());
                System.out.println(b.getVendorExtensions());
            });
        });
    }

    @Override
    public boolean supports(DocumentationType documentationType) {
        return true;
    }
}
