package com.github.xiaoymin.knife4j.spring.plugin;

import com.github.xiaoymin.knife4j.annotations.ApiMockDefine;
import com.github.xiaoymin.knife4j.annotations.ApiMockDefines;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import springfox.documentation.service.ListVendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.OperationBuilderPlugin;
import springfox.documentation.spi.service.contexts.OperationContext;

import java.util.List;
import java.util.Map;

/**
 * @author TheNow
 */
@Component
@Order(Ordered.HIGHEST_PRECEDENCE + 103)
public class OperationMockRuleReader implements OperationBuilderPlugin {
    @Override
    public void apply(OperationContext context) {
        context.operationBuilder().extensions(Lists.newArrayList(new ListVendorExtension<>("x-rules", Lists.newArrayList())));
//        List<Map<String, String>> rules = Lists.newArrayList();
//        if (context.findAnnotation(ApiMockDefines.class).isPresent()) {
//            for (ApiMockDefine param : context.findAnnotation(ApiMockDefines.class).get().defines()) {
//                String name = param.name();
//                String rule = param.rule();
//                String value = param.value();
//
//                Map<String, String> m = Maps.newHashMap();
//                m.put("name", name);
//                m.put("rule", rule);
//                m.put("value", value);
//                rules.add(m);
//            }
//            context.operationBuilder().extensions(Lists.newArrayList(new ListVendorExtension<>("mock-rules", rules)));
//        }


    }

    @Override
    public boolean supports(DocumentationType documentationType) {
        return true;
    }
}
