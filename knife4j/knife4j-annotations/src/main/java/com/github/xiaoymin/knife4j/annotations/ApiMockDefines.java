package com.github.xiaoymin.knife4j.annotations;

import java.lang.annotation.*;

/**
 * @author TheNow
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ApiMockDefines {
    ApiMockDefine[] defines() default {};
}
