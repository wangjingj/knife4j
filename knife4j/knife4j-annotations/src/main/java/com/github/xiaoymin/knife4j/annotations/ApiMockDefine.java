package com.github.xiaoymin.knife4j.annotations;

import java.lang.annotation.*;

/**
 * @author TheNow
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ApiMockDefine {

    /**
     * mockjs属性名对应pojo字段名
     *
     * @return 属性名
     */
    String name();

    /**
     * mockjs生成规则
     *
     * @return 生成规则
     */
    String rule();

    /**
     * mockjs属性值
     *
     * @return 属性值
     */
    String value();
}
