package com.jykj.demo.controller;

import com.github.xiaoymin.knife4j.annotations.ApiMockDefine;
import com.github.xiaoymin.knife4j.annotations.ApiMockDefines;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.jykj.demo.vo.Department;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/demo")
@RestController
@Api(tags = "Knife4j-demo数据测试接口")
public class DemoController {

    @PostMapping
    @ApiOperation(value = "根据参数生成实体类Department的json字符串")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "接口处理成功!!!"),
            @ApiResponse(code = 500, message = "接口处理失败,请联系开发人员进行处理!!!")
    })
    @ApiOperationSupport(ignoreParameters = {"department.count"})
    public Department mock(@RequestBody Department department) {
        return Department.builder().business(department.getBusiness()).count(department.getCount()).name(department.getName()).build();
    }

    @GetMapping
    @ApiOperation("get接口测试")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "部门名称", dataType = "String", required = true, example = "技术部", paramType = "query"),
            @ApiImplicitParam(name = "age", value = "部门人数", dataType = "Integer", required = true, example = "20", paramType = "query"),
            @ApiImplicitParam(name = "business", value = "业务范围", dataType = "String", required = true, example = "技术部", paramType = "query")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "接口处理成功!!!"),
            @ApiResponse(code = 500, message = "接口处理失败,请联系开发人员进行处理!!!"),
            @ApiResponse(code = 400, message = "请求姿势不正确")
    })
   /* @ApiMockDefines(defines = {
            @ApiMockDefine(name = "name", rule = "1-10", value = "脚印科技"),
            @ApiMockDefine(name = "count", rule = "10-100", value = "16"),
            @ApiMockDefine(name = "business", rule = "1", value = "软件开发")
    })*/
    @ApiOperationSupport(ignoreParameters = {"age"})
    public Department getMock(@RequestParam String name, @RequestParam(defaultValue = "18") Integer age, @RequestParam String business) {
        return Department.builder().business(business).count(age).name(name).build();
    }
}
