package com.jykj.demo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.Extension;
import io.swagger.annotations.ExtensionProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author TheNow
 */
@ApiModel("部门对象")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Department {

    @ApiModelProperty(required = true, notes = "部门名称", example = "技术部")
    private String name;
    @ApiModelProperty(required = true, notes = "部门人数", example = "13")
    private Integer count;
    @ApiModelProperty(required = true, notes = "部门业务范围12", example = "软件开发1",dataType="string", extensions = {@Extension(name = "fuck", properties = {
            @ExtensionProperty(name = "you", value = "him")
    })})
    private String business;

    @Override
    public String toString() {
        return "Department{" +
                "name='" + name + '\'' +
                ", count=" + count +
                ", business='" + business + '\'' +
                '}';
    }
}
